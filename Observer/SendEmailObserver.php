<?php

namespace Androo\InvoiceSend\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Psr\Log\LoggerInterface;

/**
 * Class SendEmailObserver
 * @package Androo\InvoiceSend\Observer
 *
 * Responsible for sending an invoice email whenever an invoice is registered to an order
 */
class SendEmailObserver implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var InvoiceSender
     */
    private $invoiceSender;

    /**
     * @param LoggerInterface $logger
     * @param InvoiceSender $invoiceSender
     */
    public function __construct(
        LoggerInterface $logger,
        InvoiceSender $invoiceSender
    ) {
        $this->logger = $logger;
        $this->invoiceSender = $invoiceSender;
    }

    /**
     * Send order and invoice email.
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var  Invoice $invoice */
        $invoice = $observer->getEvent()->getInvoice();

        try {
            if (!$invoice->getEmailSent()) {
                $this->invoiceSender->send($invoice);
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
    }
}
